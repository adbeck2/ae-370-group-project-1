import matplotlib.pyplot as plt
import numpy as np

# u = np.load('data/binary-sys-u.npy')
# times = np.load('data/binary-sys-times.npy')
u = np.load('data/binary-sysextend-u.npy')
times = np.load('data/binary-sysextend-times.npy')

rxpl = []
rypl = []
rzpl = []

for i in range(len(times)):
    rxpl.append(u[i][2][0])
    rypl.append(u[i][2][1])
    rzpl.append(u[i][2][2])

rxstar1 = []
rystar1 = []
rzstar1 = []

for i in range(len(times)):
    rxstar1.append(u[i][1][0])
    rystar1.append(u[i][1][1])
    rzstar1.append(u[i][1][2])

rxstar0 = []
rystar0 = []
rzstar0 = []

for i in range(len(times)):
    rxstar0.append(u[i][0][0])
    rystar0.append(u[i][0][1])
    rzstar0.append(u[i][0][2])

# plt.rcParams['figure.figsize'] = [20, 5]
# plt.plot(rxstar0, rystar0, label = 'Star 0')
# plt.plot(rxstar0, rystar0, label = 'Star 1')

import matplotlib.pyplot as plt

plt.rcParams['figure.figsize'] = [10, 10]
plt.xlabel('rx [m]')
plt.ylabel('ry [m]')
plt.title('x-y Plane')
plt.plot(rxpl, rypl, label = 'TOI 1338 b')
plt.plot(rxstar0, rystar0, label = 'TOI 1338 A')
plt.plot(rxstar1, rystar1, label = 'TOI 1338 B')

plt.legend()
plt.show()


plt.rcParams['figure.figsize'] = [10, 10]
plt.xlabel('ry [m]')
plt.ylabel('rz [m]')
plt.title('y-z Plane')
plt.plot(rypl, rzpl, label = 'TOI 1338 b')
plt.plot(rystar0, rzstar0, label = 'TOI 1338 A')
plt.plot(rystar1, rzstar1, label = 'TOI 1338 B')

plt.legend()
plt.show()

plt.rcParams['figure.figsize'] = [10, 10]
plt.xlabel('rx [m]')
plt.ylabel('rz [m]')
plt.title('x-z Plane')
plt.plot(rxpl, rzpl, label = 'TOI 1338 b')
plt.plot(rxstar0, rzstar0, label = 'TOI 1338 A')
plt.plot(rxstar1, rzstar1, label = 'TOI 1338 B')

plt.legend()
plt.show()

plt.figure()
plt.axes(projection='3d')
# plt.plot(r0x, r0y, r0z)
plt.plot(rxstar0, rystar0, rzstar0, label = 'TOI 1338 A')
plt.plot(rxstar1, rystar1, rzstar1, label = 'TOI 1338 B')
plt.plot(rxpl, rypl, rzpl, label = 'TOI 1338 b')

plt.legend()
plt.show()


