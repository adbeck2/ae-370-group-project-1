# Check terminal error using different methods
import numpy as np
from core_functions import *
import matplotlib.pyplot as plt


masses = [5.972e24, 419725]

#u_0 = np.array([[0,0,0,0,0,0], [414000 + 6378100, 0, 0, 0, 7662, 0], [363300e3, 0, 0, 0, 1081, 0]])
u_0 = np.array([[0,0,0,0,0,0], [4214929.7, 0, 5326067, 0, 7662, 0]])
u_0 = np.array([[0,0,0,0,0,0], [414000+6378100, 0, 0, 0, 7660.6717, 0]])

T = 5570.79

delta_t = 10

from datetime import datetime
start_time_RK4 = datetime.now()


u, times = ivp_RK4(u_0, T, delta_t, masses)

term_diff_RK4 = u[0][1] - u[len(u)-1][1]
term_err_RK4 = np.linalg.norm(np.array([term_diff_RK4[0], term_diff_RK4[1], term_diff_RK4[2]]))

end_time_RK4 = datetime.now()


start_time_euler = datetime.now()

u, times = ivp_forward_euler(u_0, T, delta_t, masses)

term_diff_euler = u[0][1] - u[len(u)-1][1]
term_err_euler = np.linalg.norm(np.array([term_diff_euler[0], term_diff_euler[1], term_diff_euler[2]]))

end_time_euler = datetime.now()


start_time_RK8 = datetime.now()

u, times = ivp_RK8(u_0, T, delta_t, masses)

term_diff_RK8 = u[0][1] - u[len(u)-1][1]
term_err_RK8 = np.linalg.norm(np.array([term_diff_RK8[0], term_diff_RK8[1], term_diff_RK8[2]]))

end_time_RK8 = datetime.now()

print(term_err_RK4)
print(term_err_euler)
print(term_err_RK8)

print('Duration: {}'.format(end_time_RK4 - start_time_RK4))
print('Duration: {}'.format(end_time_euler - start_time_euler))
print('Duration: {}'.format(end_time_RK8 - start_time_RK8))