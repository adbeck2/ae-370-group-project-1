import numpy as np
from core_functions import *


masses = [5.972e24, 419725, 7.34767309e22]

#u_0 = np.array([[0,0,0,0,0,0], [414000 + 6378100, 0, 0, 0, 7662, 0], [363300e3, 0, 0, 0, 1081, 0]])
u_0 = np.array([[0,0,0,0,0,0], [4214929.7, 0, 5326067, 0, 7662, 0], [361836242.8, 0, 32579493.6, 0, 1081, 0]])

T = 120000

delta_t_baseline = 1
steps = [500,250,100,50,25,10,5]

RK4_errors = []

for i in range(len(steps)):
    
    err = ivp_RK4_error(u_0, T, steps[i], delta_t_baseline, masses)
    
    RK4_errors.append(err)

RK4_errors = np.array(RK4_errors)

np.save('data/earth-errors', RK4_errors)


