import numpy as np

# True Acceleration Function
def f_true(u_ki, index, u_k, masses):
    G = 6.6743e-11
    r1 = np.array([u_ki[0], u_ki[1], u_ki[2]])
    
    a = np.zeros(3)
    for h in range(len(u_k)):
        if h == index:
            continue
        m2 = masses[h]
        r2 = np.array([u_k[h][0], u_k[h][1], u_k[h][2]])
        a_temp = G*m2*(r2-r1) / (np.linalg.norm(r2-r1)**3)
        a += a_temp

    xdot = np.array([u_ki[3], u_ki[4], u_ki[5], a[0], a[1], a[2]])

    return xdot

# MUST CHANGE TO CALCULATE FOR EACH DISCRETE OBJECT
def RK4(u_ki, index, u_k, delta_t, masses):

    k1 = delta_t * f_true(u_ki, index, u_k, masses)
    k2 = delta_t * f_true(u_ki + k1/2, index, u_k, masses)
    k3 = delta_t * f_true(u_ki + k2/2, index, u_k, masses)
    k4 = delta_t * f_true(u_ki + k3, index, u_k, masses)
        
    return u_ki + (1/6) * (k1 + 2*k2 + 2*k3 + k4)

#Includes Collision
def ivp_RK4(u_0, T, delta_t, masses, radii=None):
    u = [u_0]

    check_collision = None
    if radii != None:
        check_collision = True
    
    times = np.arange(0,T+delta_t,delta_t)
    
    for i in range(int(T/delta_t)):
        previous = u[i]

        update = []
        for j in range(len(previous)):
            update_temp = RK4(previous[j], j, previous, delta_t, masses)  #UPDATES EACH OBJECT
            update.append(update_temp)

        update = np.array(update)
        u.append(update)
        timing_percentage = times[i] / T * 100
        print(str(timing_percentage) + ' Percent Completed')

        collision = None
        if check_collision == True:
            for p in range(len(masses)):
                for q in range(len(masses)):
                    if p == q:
                        continue
                    radius = radii[p] + radii[q] 
                    radius_coords = np.array([update[p][0], update[p][1], update[p][2]]) - np.array([update[q][0], update[q][1], update[q][2]])
                    radius_mag = np.linalg.norm(radius_coords)
                    if radius_mag <= radius:
                        collision = True
                        indices = np.array([p,q])
        if collision == True:
            print('Collision Has Occurred')
            print('Indices of collision are ' + str(indices[0]) + ' and ' + str(indices[1]))
            break
        
    u = np.array(u)
    
    return u, times

def ivp_RK4_error(u_0, T, delta_t, delta_t_baseline, masses):
    
    u, times = ivp_RK4(u_0, T, delta_t, masses)
    
    u_base, times_base = ivp_RK4(u_0, T, delta_t_baseline, masses)
    
    err = (np.linalg.norm(u[len(u)-1] - u_base[len(u_base)-1])) / np.linalg.norm(u_base[len(u_base)-1])
    
    return err


def forward_euler(u_ki, index, u_k, delta_t, masses):
    
    u_kplus1 = u_ki + delta_t*f_true(u_ki, index, u_k, masses)
    return u_kplus1

def ivp_forward_euler(u_0, T, delta_t, masses):
    u = [u_0]
    
    times = np.arange(0,T+delta_t,delta_t)
    
    for i in range(int(T/delta_t)):
        previous = u[i]

        update = []
        for j in range(len(previous)):
            update_temp = forward_euler(previous[j], j, previous, delta_t, masses)  #UPDATES EACH OBJECT
            update.append(update_temp)

        update = np.array(update)
        u.append(update)
        timing_percentage = times[i] / T * 100
        print(str(timing_percentage) + ' Percent Completed')
        
    u = np.array(u)
    
    return u, times

def ivp_forward_euler_error(u_0, T, delta_t, delta_t_baseline, masses):
    
    u, times = ivp_forward_euler(u_0, T, delta_t, masses)
    
    u_base, times_base = ivp_forward_euler(u_0, T, delta_t_baseline, masses)
    
    err = (np.linalg.norm(u[len(u)-1] - u_base[len(u_base)-1])) / np.linalg.norm(u_base[len(u_base)-1])
    
    return err





C_Coef = [0, .5, .5, .5 + np.sqrt(21)/ 14, .5 + np.sqrt(21)/ 14, .5, .5 - np.sqrt(21)/ 14, .5 - np.sqrt(21)/ 14, .5, .5 + np.sqrt(21)/ 14, 1]

B = [.05, 0, 0, 0, 0, 0, 0, 49/180, 16/45, 49/180, .05]

a = np.zeros((12,12))
a[2,1]=1/2
a[3,1]=1/4
a[3,2]=1/4
a[4,1]=1/7
a[4,2]=-1/14-3/98*21**(1/2)
a[4,3]=3/7+5/49*21**(1/2)
a[5,1]=11/84+1/84*21**(1/2)
a[5,2]=0
a[5,3]=2/7+4/63*21**(1/2)
a[5,4]=1/12-1/252*21**(1/2)
a[6,1]=5/48+1/48*21**(1/2)
a[6,2]=0
a[6,3]=1/4+1/36*21**(1/2)
a[6,4]=-77/120+7/180*21**(1/2)
a[6,5]=63/80-7/80*21**(1/2)
a[7,1]=5/21-1/42*21**(1/2)
a[7,2]=0
a[7,3]=-48/35+92/315*21**(1/2)
a[7,4]=211/30-29/18*21**(1/2)
a[7,5]=-36/5+23/14*21**(1/2)
a[7,6]=9/5-13/35*21**(1/2)
a[8,1]=1/14
a[8,2]=0
a[8,3]=0
a[8,4]=0
a[8,5]=1/9-1/42*21**(1/2)
a[8,6]=13/63-1/21*21**(1/2)
a[8,7]=1/9
a[9,1]=1/32
a[9,2]=0
a[9,3]=0
a[9,4]=0
a[9,5]=91/576-7/192*21**(1/2)
a[9,6]=11/72
a[9,7]=-385/1152-25/384*21**(1/2)
a[9,8]=63/128+13/128*21**(1/2)
a[10,1]=1/14
a[10,2]=0
a[10,3]=0
a[10,4]=0
a[10,5]=1/9
a[10,6]=-733/2205-1/15*21**(1/2)
a[10,7]=515/504+37/168*21**(1/2)
a[10,8]=-51/56-11/56*21**(1/2)
a[10,9]=132/245+4/35*21**(1/2)
a[11,1]=0
a[11,2]=0
a[11,3]=0
a[11,4]=0
a[11,5]=-7/3+7/18*21**(1/2)
a[11,6]=-2/5+28/45*21**(1/2)
a[11,7]=-91/24-53/72*21**(1/2)
a[11,8]=301/72+53/72*21**(1/2)
a[11,9]=28/45-28/45*21**(1/2)
a[11,10]=49/18-7/18*21**(1/2)
A_Coef = a[1:, 1:]

def RK8(u_ki, index, u_k, delta_t, masses):

    def k1(u_ki, index, u_k, delta_t, masses):
        return f_true(u_ki, index, u_k, masses)

    def k2(u_ki, index, u_k, delta_t, masses):
        return f_true(u_ki + delta_t * (a[1, 0] * k1(u_ki, index, u_k, delta_t, masses)), index, u_k, masses)

    def k3(u_ki, index, u_k, delta_t, masses):
        return f_true(u_ki + delta_t * (a[2, 0] * k1(u_ki, index, u_k, delta_t, masses) + a[2, 1] * k2(u_ki, index, u_k, delta_t, masses)), index, u_k, masses)

    def k4(u_ki, index, u_k, delta_t, masses):
        return f_true(u_ki + delta_t * (a[3, 0] * k1(u_ki, index, u_k, delta_t, masses) + a[3, 1] * k2(u_ki, index, u_k, delta_t, masses) + a[3, 2] * k3(u_ki, index, u_k, delta_t, masses)), index, u_k, masses)

    def k5(u_ki, index, u_k, delta_t, masses):
        return f_true(u_ki + delta_t * (a[4, 0] * k1(u_ki, index, u_k, delta_t, masses) + a[4, 1] * k2(u_ki, index, u_k, delta_t, masses) + a[4, 2] * k3(u_ki, index, u_k, delta_t, masses) + a[4, 3] * k4(u_ki, index, u_k, delta_t, masses)), index, u_k, masses)

    def k6(u_ki, index, u_k, delta_t, masses):
        return f_true(u_ki + delta_t * (a[5, 0] * k1(u_ki, index, u_k, delta_t, masses) + a[5, 1] * k2(u_ki, index, u_k, delta_t, masses) + a[5, 2] * k3(u_ki, index, u_k, delta_t, masses) + a[5, 3] * k4(u_ki, index, u_k, delta_t, masses) + a[5, 4] * k5(u_ki, index, u_k, delta_t, masses)), index, u_k, masses)

    def k7(u_ki, index, u_k, delta_t, masses):
        return f_true(u_ki + delta_t * (a[6, 0] * k1(u_ki, index, u_k, delta_t, masses) + a[6, 1] * k2(u_ki, index, u_k, delta_t, masses) + a[6, 2] * k3(u_ki, index, u_k, delta_t, masses) + a[6, 3] * k4(u_ki, index, u_k, delta_t, masses) + a[6, 4] * k5(u_ki, index, u_k, delta_t, masses) + a[6, 5] * k6(u_ki, index, u_k, delta_t, masses)), index, u_k, masses)

    def k8(u_ki, index, u_k, delta_t, masses):
        return f_true(u_ki + delta_t * (a[7, 0] * k1(u_ki, index, u_k, delta_t, masses) + a[7, 1] * k2(u_ki, index, u_k, delta_t, masses) + a[7, 2] * k3(u_ki, index, u_k, delta_t, masses) + a[7, 3] * k4(u_ki, index, u_k, delta_t, masses) + a[7, 4] * k5(u_ki, index, u_k, delta_t, masses) + a[7, 5] * k6(u_ki, index, u_k, delta_t, masses) + a[7, 6] * k7(u_ki, index, u_k, delta_t, masses)), index, u_k, masses)

    def k9(u_ki, index, u_k, delta_t, masses):
        return f_true(u_ki + delta_t * (a[8, 0] * k1(u_ki, index, u_k, delta_t, masses) + a[8, 1] * k2(u_ki, index, u_k, delta_t, masses) + a[8, 2] * k3(u_ki, index, u_k, delta_t, masses) + a[8, 3] * k4(u_ki, index, u_k, delta_t, masses) + a[8, 4] * k5(u_ki, index, u_k, delta_t, masses) + a[8, 5] * k6(u_ki, index, u_k, delta_t, masses) + a[8, 6] * k7(u_ki, index, u_k, delta_t, masses) + a[8, 7] * k8(u_ki, index, u_k, delta_t, masses)), index, u_k, masses)

    def k10(u_ki, index, u_k, delta_t, masses):
        return f_true(u_ki + delta_t * (a[9, 0] * k1(u_ki, index, u_k, delta_t, masses) + a[9, 1] * k2(u_ki, index, u_k, delta_t, masses) + a[9, 2] * k3(u_ki, index, u_k, delta_t, masses) + a[9, 3] * k4(u_ki, index, u_k, delta_t, masses) + a[9, 4] * k5(u_ki, index, u_k, delta_t, masses) + a[9, 5] * k6(u_ki, index, u_k, delta_t, masses) + a[9, 6] * k7(u_ki, index, u_k, delta_t, masses) + a[9, 7] * k8(u_ki, index, u_k, delta_t, masses) + a[9, 8] * k9(u_ki, index, u_k, delta_t, masses)), index, u_k, masses)

    def k11(u_ki, index, u_k, delta_t, masses):
        return f_true(u_ki + delta_t * (a[10, 0] * k1(u_ki, index, u_k, delta_t, masses) + a[10, 1] * k2(u_ki, index, u_k, delta_t, masses) + a[10, 2] * k3(u_ki, index, u_k, delta_t, masses) + a[10, 3] * k4(u_ki, index, u_k, delta_t, masses) + a[10, 4] * k5(u_ki, index, u_k, delta_t, masses) + a[10, 5] * k6(u_ki, index, u_k, delta_t, masses) + a[10, 6] * k7(u_ki, index, u_k, delta_t, masses) + a[10, 7] * k8(u_ki, index, u_k, delta_t, masses) + a[10, 8] * k9(u_ki, index, u_k, delta_t, masses) + a[10, 9] * k10(u_ki, index, u_k, delta_t, masses)), index, u_k, masses)

    return u_ki + delta_t*(B[0]*k1(u_ki, index, u_k, delta_t, masses) + B[1]*k2(u_ki, index, u_k, delta_t, masses) + B[2]*k3(u_ki, index, u_k, delta_t, masses) + B[3]*k4(u_ki, index, u_k, delta_t, masses) + B[4]*k5(u_ki, index, u_k, delta_t, masses) + B[5]*k6(u_ki, index, u_k, delta_t, masses) + B[6]*k7(u_ki, index, u_k, delta_t, masses) + B[7]*k8(u_ki, index, u_k, delta_t, masses)  + B[8]*k9(u_ki, index, u_k, delta_t, masses) + B[9]*k10(u_ki, index, u_k, delta_t, masses) + B[10]*k11(u_ki, index, u_k, delta_t, masses))

def ivp_RK8(u_0, T, delta_t, masses):
    u = [u_0]
    
    times = np.arange(0,T+delta_t,delta_t)
    
    for i in range(int(T/delta_t)):
        previous = u[i]
        
        update = []
        for j in range(len(previous)):
            t_n = T + j * delta_t
            update_temp = RK8(previous[j], j, previous, delta_t, masses)  #UPDATES EACH OBJECT
            update.append(update_temp)

        update = np.array(update)
        u.append(update)
        
    u = np.array(u)
    
    return u, times