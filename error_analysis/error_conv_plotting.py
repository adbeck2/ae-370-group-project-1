import numpy as np
import matplotlib.pyplot as plt

delta_t_baseline = 1
steps = [500,250,100,50,25,10,5]
RK4_errors = np.load('data/earth-errors.npy')

##Error Graph
plt.title('Error')
plt.plot(steps, RK4_errors, label = 'RK4', marker = '^')

x = [steps[6], steps[0]]
y_RK4 = [RK4_errors[6], RK4_errors[6] * ((steps[0]/steps[6])**4)]

plt.plot(x, y_RK4, color = 'brown', linestyle='dotted', label = 'O(delta_t^4)')

plt.xlabel('Time Step [s]')
plt.ylabel('Error')
plt.yscale('log')
plt.xscale('log')
plt.legend()
plt.show()