#Modeling only the ISS using euler
from datetime import datetime
start_time = datetime.now()
import numpy as np
from core_functions import *


masses = [5.972e24, 419725]

#u_0 = np.array([[0,0,0,0,0,0], [414000 + 6378100, 0, 0, 0, 7662, 0], [363300e3, 0, 0, 0, 1081, 0]])
u_0 = np.array([[0,0,0,0,0,0], [4214929.7, 0, 5326067, 0, 7662, 0]])

T = 2360600

delta_t = 10

u, times = ivp_forward_euler(u_0, T, delta_t, masses)

np.save('data/iss-euler-u', u)
np.save('data/iss-euler-times', times)

end_time = datetime.now()
print('Duration: {}'.format(end_time - start_time))