import matplotlib.pyplot as plt
import numpy as np

u = np.load('data/jupiter-sys-u.npy')
times = np.load('data/jupiter-sys-times.npy')

rxjupiter = []
ryjupiter = []
rzjupiter = []

for i in range(len(times)):
    rxjupiter.append(u[i][0][0])
    ryjupiter.append(u[i][0][1])
    rzjupiter.append(u[i][0][2])

rxjupiter = np.array(rxjupiter)
ryjupiter = np.array(ryjupiter)
rzjupiter = np.array(rzjupiter)

rxio = []
ryio = []
rzio = []

for i in range(len(times)):
    rxio.append(u[i][1][0])
    ryio.append(u[i][1][1])
    rzio.append(u[i][1][2])

rxio = np.array(rxio)
ryio = np.array(ryio)
rzio = np.array(rzio)

rxgany = []
rygany = []
rzgany = []

for i in range(len(times)):
    rxgany.append(u[i][2][0])
    rygany.append(u[i][2][1])
    rzgany.append(u[i][2][2])

rxgany = np.array(rxgany)
rygany = np.array(rygany)
rzgany = np.array(rzgany)

rxeuropa = []
ryeuropa = []
rzeuropa = []

for i in range(len(times)):
    rxeuropa.append(u[i][3][0])
    ryeuropa.append(u[i][3][1])
    rzeuropa.append(u[i][3][2])

rxeuropa = np.array(rxeuropa)
ryeuropa = np.array(ryeuropa)
rzeuropa = np.array(rzeuropa)

#callisto
rxcallisto = []
rycallisto = []
rzcallisto = []

for i in range(len(times)):
    rxcallisto.append(u[i][4][0])
    rycallisto.append(u[i][4][1])
    rzcallisto.append(u[i][4][2])

rxcallisto = np.array(rxcallisto)
rycallisto = np.array(rycallisto)
rzcallisto = np.array(rzcallisto)

#amalthea
rxamalthea = []
ryamalthea = []
rzamalthea = []

for i in range(len(times)):
    rxamalthea.append(u[i][5][0])
    ryamalthea.append(u[i][5][1])
    rzamalthea.append(u[i][5][2])

rxamalthea = np.array(rxamalthea)
ryamalthea = np.array(ryamalthea)
rzamalthea = np.array(rzamalthea)

#thebe
rxthebe = []
rythebe = []
rzthebe = []

for i in range(len(times)):
    rxthebe.append(u[i][6][0])
    rythebe.append(u[i][6][1])
    rzthebe.append(u[i][6][2])

rxthebe = np.array(rxthebe)
rythebe = np.array(rythebe)
rzthebe = np.array(rzthebe)

#himalia
rxhimalia = []
ryhimalia = []
rzhimalia = []

for i in range(len(times)):
    rxhimalia.append(u[i][7][0])
    ryhimalia.append(u[i][7][1])
    rzhimalia.append(u[i][7][2])

rxhimalia = np.array(rxhimalia)
ryhimalia = np.array(ryhimalia)
rzhimalia = np.array(rzhimalia)

#juno
rxjuno = []
ryjuno = []
rzjuno = []

for i in range(len(times)):
    rxjuno.append(u[i][8][0])
    ryjuno.append(u[i][8][1])
    rzjuno.append(u[i][8][2])

rxjuno = np.array(rxjuno)
ryjuno = np.array(ryjuno)
rzjuno = np.array(rzjuno)

plt.rcParams['figure.figsize'] = [10, 10]
plt.xlabel('rx [m]')
plt.ylabel('ry [m]')
plt.title('x-y Plane')
plt.plot(rxgany, rygany, label = 'Ganymede')
plt.plot(rxio, ryio, label = 'Io')
plt.plot(rxjupiter, ryjupiter, label = 'Jupiter')
plt.plot(rxeuropa, ryeuropa, label = 'Europa')
plt.plot(rxcallisto, rycallisto, label = 'Callisto')
plt.plot(rxamalthea, ryamalthea, label = 'Amalthea')
plt.plot(rxthebe, rythebe, label = 'Thebe')
plt.plot(rxhimalia, ryhimalia, label = 'Himalia')
plt.plot(rxjuno, ryjuno, label = 'Juno')

plt.legend()
plt.show()


plt.rcParams['figure.figsize'] = [10, 10]
plt.xlabel('ry [m]')
plt.ylabel('rz [m]')
plt.title('y-z Plane')
plt.plot(rygany, rzgany, label = 'Ganymede')
plt.plot(ryio, rzio, label = 'Io')
plt.plot(ryjupiter, rzjupiter, label = 'Jupiter')
plt.plot(ryeuropa, rzeuropa, label = 'Europa')
plt.plot(rycallisto, rzcallisto, label = 'Callisto')
plt.plot(ryamalthea, rzamalthea, label = 'Amalthea')
plt.plot(rythebe, rzthebe, label = 'Thebe')
plt.plot(ryhimalia, rzhimalia, label = 'Himalia')
plt.plot(ryjuno, rzjuno, label = 'Juno')

plt.legend()
plt.show()

plt.rcParams['figure.figsize'] = [10, 10]
plt.xlabel('rx [m]')
plt.ylabel('rz [m]')
plt.title('x-z Plane')
plt.plot(rxgany, rzgany, label = 'Ganymede')
plt.plot(rxio, rzio, label = 'Io')
plt.plot(rxjupiter, rzjupiter, label = 'Jupiter')
plt.plot(rxeuropa, rzeuropa, label = 'Europa')
plt.plot(rxcallisto, rzcallisto, label = 'Callisto')
plt.plot(rxamalthea, rzamalthea, label = 'Amalthea')
plt.plot(rxthebe, rzthebe, label = 'Thebe')
plt.plot(rxhimalia, rzhimalia, label = 'Himalia')
plt.plot(rxjuno, rzjuno, label = 'Juno')

plt.legend()
plt.show()


# plt.plot(rxio, ryio)
# plt.show()

# plt.plot(rxio, rzio)
# plt.show()

from mpl_toolkits.mplot3d import Axes3D
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot(rxgany/1000,rygany/1000,rzgany/1000, label = 'Ganymede')
ax.plot(rxio/1000, ryio/1000, rzio/1000, label = 'Io')
ax.plot(rxjupiter/1000, ryjupiter/1000, rzjupiter/1000, label = 'Jupiter')
ax.plot(rxeuropa/1000, ryeuropa/1000, rzeuropa/1000, label = 'Europa')
ax.plot(rxcallisto/1000, rycallisto/1000, rzcallisto/1000, label = 'Callisto')
ax.plot(rxamalthea/1000, ryamalthea/1000, rzamalthea/1000, label = 'Amalthea')
ax.plot(rxthebe/1000, rythebe/1000, rzthebe/1000, label = 'Thebe')
# ax.plot(rxhimalia/1000, ryhimalia/1000, rzhimalia/1000, label = 'Himalia')
ax.plot(rxjuno/1000, ryjuno/1000, rzjuno/1000, label = 'Juno (Spacecraft)')
ax.set_title('3d Jupiter System')
ax.set_xlabel('rx [km]')
ax.set_ylabel('ry [km]')
ax.set_zlabel('rz [km]')
plt.legend()
plt.show()


############
#Animated Plot
from matplotlib import animation
# Time Array


rxganyloose = []
ryganyloose = []
rzganyloose = []
timesloose = []

for i in range(int(len(times)/1000)):
    rxganyloose.append(rxgany[i*1000])
    ryganyloose.append(rygany[i*1000])
    rzganyloose.append(rzgany[i*1000])
    timesloose.append(times[i*1000])

rxganyloose = np.array(rxganyloose)
ryganyloose = np.array(ryganyloose)
rzganyloose = np.array(rzganyloose)
timesloose = np.array(timesloose)

rxjupiterloose = []
ryjupiterloose = []
rzjupiterloose = []

for i in range(int(len(times)/1000)):
    rxjupiterloose.append(rxjupiter[i*1000])
    ryjupiterloose.append(ryjupiter[i*1000])
    rzjupiterloose.append(rzjupiter[i*1000])

rxjupiterloose = np.array(rxjupiterloose)
ryjupiterloose = np.array(ryjupiterloose)
rzjupiterloose = np.array(rzjupiterloose)

rxioloose = []
ryioloose = []
rzioloose = []

for i in range(int(len(times)/1000)):
    rxioloose.append(rxio[i*1000])
    ryioloose.append(ryio[i*1000])
    rzioloose.append(rzio[i*1000])

rxioloose = np.array(rxioloose)
ryioloose = np.array(ryioloose)
rzioloose = np.array(rzioloose)

rxeuropaloose = []
ryeuropaloose = []
rzeuropaloose = []

for i in range(int(len(times)/1000)):
    rxeuropaloose.append(rxeuropa[i*1000])
    ryeuropaloose.append(ryeuropa[i*1000])
    rzeuropaloose.append(rzeuropa[i*1000])

rxeuropaloose = np.array(rxeuropaloose)
ryeuropaloose = np.array(ryeuropaloose)
rzeuropaloose = np.array(rzeuropaloose)

rxcallistoloose = []
rycallistoloose = []
rzcallistoloose = []

for i in range(int(len(times)/1000)):
    rxcallistoloose.append(rxcallisto[i*1000])
    rycallistoloose.append(rycallisto[i*1000])
    rzcallistoloose.append(rzcallisto[i*1000])

rxcallistoloose = np.array(rxcallistoloose)
rycallistoloose = np.array(rycallistoloose)
rzcallistoloose = np.array(rzcallistoloose)

rxamalthealoose = []
ryamalthealoose = []
rzamalthealoose = []

for i in range(int(len(times)/1000)):
    rxamalthealoose.append(rxamalthea[i*1000])
    ryamalthealoose.append(ryamalthea[i*1000])
    rzamalthealoose.append(rzamalthea[i*1000])

rxamalthealoose = np.array(rxamalthealoose)
ryamalthealoose = np.array(ryamalthealoose)
rzamalthealoose = np.array(rzamalthealoose)

rxthebeloose = []
rythebeloose = []
rzthebeloose = []

for i in range(int(len(times)/1000)):
    rxthebeloose.append(rxthebe[i*1000])
    rythebeloose.append(rythebe[i*1000])
    rzthebeloose.append(rzthebe[i*1000])

rxthebeloose = np.array(rxthebeloose)
rythebeloose = np.array(rythebeloose)
rzthebeloose = np.array(rzthebeloose)

rxjunoloose = []
ryjunoloose = []
rzjunoloose = []

for i in range(int(len(times)/1000)):
    rxjunoloose.append(rxjuno[i*1000])
    ryjunoloose.append(ryjuno[i*1000])
    rzjunoloose.append(rzjuno[i*1000])

rxjunoloose = np.array(rxjunoloose)
ryjunoloose = np.array(ryjunoloose)
rzjunoloose = np.array(rzjunoloose)

# Setting up Data Set for Animation
t = timesloose
datasetgany = np.array([rxganyloose/1000, ryganyloose/1000, rzganyloose/1000])
datasetjupiter = np.array([rxjupiterloose/1000, ryjupiterloose/1000, rzjupiterloose/1000])
datasetio = np.array([rxioloose/1000, ryioloose/1000, rzioloose/1000])
dataseteuropa = np.array([rxeuropaloose/1000, ryeuropaloose/1000, rzeuropaloose/1000])
datasetcallisto = np.array([rxcallistoloose/1000, rycallistoloose/1000, rzcallistoloose/1000])
datasetamalthea = np.array([rxamalthealoose/1000, ryamalthealoose/1000, rzamalthealoose/1000])
datasetthebe = np.array([rxthebeloose/1000, rythebeloose/1000, rzthebeloose/1000])
datasetjuno = np.array([rxjunoloose/1000, ryjunoloose/1000, rzjunoloose/1000])
numDataPoints = len(t)

def animate_func(num):
    ax.clear()  # Clears the figure to update the line, point,   
                # title, and axes
    # Updating Trajectory Line (num+1 due to Python indexing)
    ax.plot3D(datasetgany[0, :num+1], datasetgany[1, :num+1], 
              datasetgany[2, :num+1], c='blue')
    ax.plot3D(datasetjupiter[0, :num+1], datasetjupiter[1, :num+1], 
              datasetjupiter[2, :num+1], c='green')
    ax.plot3D(datasetio[0, :num+1], datasetio[1, :num+1], 
              datasetio[2, :num+1], c='orange')
    ax.plot3D(dataseteuropa[0, :num+1], dataseteuropa[1, :num+1], 
              dataseteuropa[2, :num+1], c='red')
    ax.plot3D(datasetcallisto[0, :num+1], datasetcallisto[1, :num+1], 
              datasetcallisto[2, :num+1], c='blueviolet')
    ax.plot3D(datasetamalthea[0, :num+1], datasetamalthea[1, :num+1], 
              datasetamalthea[2, :num+1], c='sienna')
    ax.plot3D(datasetthebe[0, :num+1], datasetthebe[1, :num+1], 
              datasetthebe[2, :num+1], c='magenta')
    ax.plot3D(datasetjuno[0, :num+1], datasetjuno[1, :num+1], 
              datasetjuno[2, :num+1], c='goldenrod')
    # Updating Point Location 
    ax.scatter(datasetgany[0, num], datasetgany[1, num], datasetgany[2, num], 
               c='blue', marker='o')
    ax.scatter(datasetjupiter[0, num], datasetjupiter[1, num], datasetjupiter[2, num], 
               c='green', marker='o')
    ax.scatter(datasetio[0, num], datasetio[1, num], datasetio[2, num], 
               c='orange', marker='o')
    ax.scatter(dataseteuropa[0, num], dataseteuropa[1, num], dataseteuropa[2, num], 
               c='red', marker='o')
    ax.scatter(datasetcallisto[0, num], datasetcallisto[1, num], datasetcallisto[2, num], 
               c='blueviolet', marker='o')
    ax.scatter(datasetamalthea[0, num], datasetamalthea[1, num], datasetamalthea[2, num], 
               c='sienna', marker='o')
    ax.scatter(datasetthebe[0, num], datasetthebe[1, num], datasetthebe[2, num], 
               c='magenta', marker='o')
    ax.scatter(datasetjuno[0, num], datasetjuno[1, num], datasetjuno[2, num], 
               c='goldenrod', marker='o')
    # Adding Constant Origin
    ax.plot3D(datasetgany[0, 0], datasetgany[1, 0], datasetgany[2, 0],     
               c='black', marker='o')
    ax.plot3D(datasetjupiter[0, 0], datasetjupiter[1, 0], datasetjupiter[2, 0],     
               c='black', marker='o')
    ax.plot3D(datasetio[0, 0], datasetio[1, 0], datasetio[2, 0],     
               c='black', marker='o')
    ax.plot3D(dataseteuropa[0, 0], dataseteuropa[1, 0], dataseteuropa[2, 0],     
               c='black', marker='o')
    ax.plot3D(datasetcallisto[0, 0], datasetcallisto[1, 0], datasetcallisto[2, 0],     
               c='black', marker='o')
    ax.plot3D(datasetamalthea[0, 0], datasetamalthea[1, 0], datasetamalthea[2, 0],     
               c='black', marker='o')
    ax.plot3D(datasetthebe[0, 0], datasetthebe[1, 0], datasetthebe[2, 0],     
               c='black', marker='o')
    ax.plot3D(datasetjuno[0, 0], datasetjuno[1, 0], datasetjuno[2, 0],     
               c='black', marker='o')
    # Setting Axes Limits
    ax.set_xlim3d([-4000000, 2500000])
    ax.set_ylim3d([-4000000, 2500000])
    ax.set_zlim3d([-4000000, 2500000])

    # Adding Figure Labels
    ax.set_title('Trajectory \nTime = ' + str(np.round(t[num],    
                 decimals=2)) + ' sec')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')

# Plotting the Animation
fig = plt.figure()
ax = plt.axes(projection='3d')
line_ani = animation.FuncAnimation(fig, animate_func, interval=100, frames=numDataPoints)
# f = r"data/jupiter-animation.gif"
# writergif = animation.PillowWriter(fps=numDataPoints)
# line_ani.save(f, writer=writergif)

plt.show()
