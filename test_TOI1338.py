import numpy as np
from regex import R
from core_functions import *

#sun mass: 1.9885*10^30 https://nssdc.gsfc.nasa.gov/planetary/factsheet/sunfact.html
#earth mass: 5.9724e+24
#TOI-1338 data: https://arxiv.org/abs/2004.07783 (note data is in reduced-mass frame)
#assumption: Stars are in XY plane

masses = [1.127*1.9885e+30, .3128*1.9885e+30, 33.0*5.9724e+24] #median mass in kg
# masses = [1.127*1.9885e+30, .3128*1.9885e+30] #median mass in kg

AU = 1.496e+11 #meters
G = 6.6743e-11

M = masses[0]+masses[1] #mass constant of binary stars

i_bin = np.radians(89.696) #inclination
e_bin = 0.15603
P_bin = 14.608559*3600*24 #period, s
a_bin = 0.1321*AU 
# a_0 = masses[1]/M*a_bin
# a_1 = masses[0]/M*a_bin #http://faculty.wwu.edu/vawter/PhysicsNet/Topics/Gravity/BinaryStars.html
peri_1 = a_bin*(1-e_bin**2)/(1+e_bin) ##http://faculty.wwu.edu/vawter/PhysicsNet/Topics/Gravity/BinaryStars.html
#https://web.archive.org/web/20100724224107/http://faculty.wwu.edu/vawter/PhysicsNet/Topics/Gravity/BinaryStars.html
v_1 = (2*np.pi*a_bin/P_bin)*np.sqrt((1+e_bin)*(1-e_bin))#https://web.njit.edu/~gary/320/Lecture6.html
v_0 = masses[1]*v_1/masses[0] #https://physics.stackexchange.com/questions/545303/why-does-my-binary-star-simulation-only-work-for-equal-masses-and-initial-speeds
M_bin = masses[0]+masses[1]
a_0 = masses[1]/M_bin*a_bin
r_0 = a_0*(1-e_bin**2)/(1+e_bin) #need to find center of mass for determining planet position

# v_bin = 2*np.pi*a_bin/P_bin*(1 - 1/4*e_bin**2 - 3/64*e_bin**4 - 5/256*e_bin**6) #average orbital speed, m/s 

i_pl = np.radians(89.37)-i_bin
long_ascend_pl = np.radians(0.91)

#set up new coordinate system for planetary plane
x_pl = np.array([np.cos(long_ascend_pl),np.sin(long_ascend_pl),0])
y_pl = np.array([-np.cos(i_pl)*np.sin(long_ascend_pl),np.cos(i_pl)*np.cos(long_ascend_pl),np.sin(i_pl)])
y_pl = y_pl/np.linalg.norm(y_pl)
z_pl = np.cross(x_pl,y_pl)
z_pl = z_pl/np.linalg.norm(z_pl)
R_pl = np.array([
    [x_pl[0],y_pl[0],z_pl[0]],
    [x_pl[1],y_pl[1],z_pl[1]],
    [x_pl[2],y_pl[2],z_pl[2]]
])

arg_pl = np.radians(258.6)
e_pl = 0.0880
a_pl = 0.4607*AU
P_pl = 95.174*3600*24 #s
# p_pl = a_pl*(1-e_pl**2) #semi-latus rectum
peri_pl = a_pl*(1-e_pl) #meters https://www.planetary.org/articles/3380
k = (np.sum(masses))*G #gravitational constant of system
vp_pl = np.sqrt(k*(2/peri_pl-1/a_pl)) #https://www.omnicalculator.com/physics/orbital-velocity

per_vec_pl = np.array([r_0 + peri_pl*np.cos(arg_pl), peri_pl*np.sin(arg_pl), 0])
per_vec_trans_pl = R_pl@per_vec_pl

vp_vec_pl = np.array([-vp_pl,0,0])
vp_vec_trans_pl = R_pl@vp_vec_pl
# vp_pl = np.sqrt((k_pl/p_pl)*(1+e_pl)(1-e_pl))
# v_pl = 2*np.pi*a_pl/P_pl*(1 - 1/4*e_pl**2 - 3/64*e_pl**4 - 5/256*e_pl**6) #average orbital speed, m/s

u_0 = np.array([[0,0,0,0,-v_0,0],
                [peri_1,0,0,0,v_1,0],
                [per_vec_trans_pl[0], per_vec_trans_pl[1], per_vec_trans_pl[2], vp_vec_trans_pl[0], vp_vec_trans_pl[1], vp_vec_trans_pl[2]]])

# u_0 = np.array([[0,0,0,0,-v_0,0],
#                 [peri_1,0,0,0,v_1,0]])

T = 72000000
# T = 12000000
# T = 100000

delta_t = 60
# delta_t = 10

u, times = ivp_RK4(u_0, T, delta_t, masses)

# np.save('data/binary-sys-u', u)
# np.save('data/binary-sys-times', times)

np.save('data/binary-sysextend-u', u)
np.save('data/binary-sysextend-times', times)

# #binary star 0
# r0x = []
# r0y = []
# r0z = []
# #binary star 1
# r1x = []
# r1y = []
# r1z = []
# #planet
# rpx = []
# rpy = []
# rpz = []

# for i in range(len(times)):
#     r0x.append(u[i][0][0])
#     r0y.append(u[i][0][1])
#     r0z.append(u[i][0][2])
#     r1x.append(u[i][1][0])
#     r1y.append(u[i][1][1])
#     r1z.append(u[i][1][2])
#     rpx.append(u[i][2][0])
#     rpy.append(u[i][2][1])
#     rpz.append(u[i][2][2])

# import matplotlib.pyplot as plt
# plt.axes(projection='3d')
# # plt.plot(r0x, r0y, r0z)
# plt.plot(r1x, r1y, r1z)
# # plt.plot(rpx, rpy, rpz)

# plt.show()
