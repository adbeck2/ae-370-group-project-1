# Three body system with RK4
from datetime import datetime
start_time = datetime.now()
import numpy as np
from core_functions import *


masses = [5.972e24, 419725, 7.34767309e22]
radii = [6378100, 50, 1737.4e3]

#u_0 = np.array([[0,0,0,0,0,0], [414000 + 6378100, 0, 0, 0, 7662, 0], [363300e3, 0, 0, 0, 1081, 0]])
u_0 = np.array([[0,0,0,0,0,0], [4214929.7, 0, 5326067, 0, 7662, 0], [361836242.8, 0, 32579493.6, 0, 1081, 0]])

T = 2360600

delta_t = 10

u, times = ivp_RK4(u_0, T, delta_t, masses, radii=radii)

# np.save('data/earth-sys-u', u)
# np.save('data/earth-sys-times', times)


end_time = datetime.now()
print('Duration: {}'.format(end_time - start_time))

