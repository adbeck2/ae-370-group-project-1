import matplotlib.pyplot as plt
import numpy as np

u = np.load('data/earth-euler-u.npy')
times = np.load('data/earth-euler-times.npy')

rxearth = []
ryearth = []
rzearth = []
vxearth = []
vyearth = []
vzearth = []

for i in range(len(times)):
    rxearth.append(u[i][0][0])
    ryearth.append(u[i][0][1])
    rzearth.append(u[i][0][2])
    vxearth.append(u[i][0][3])
    vyearth.append(u[i][0][4])
    vzearth.append(u[i][0][5])

rxearth = np.array(rxearth)
ryearth = np.array(ryearth)
rzearth = np.array(rzearth)
vxearth = np.array(vxearth)
vyearth = np.array(vyearth)
vzearth = np.array(vzearth)

rxsat = []
rysat = []
rzsat = []
vxsat = []
vysat = []
vzsat = []

for i in range(len(times)):
    rxsat.append(u[i][1][0])
    rysat.append(u[i][1][1])
    rzsat.append(u[i][1][2])
    vxsat.append(u[i][1][3])
    vysat.append(u[i][1][4])
    vzsat.append(u[i][1][5])

rxsat = np.array(rxsat)
rysat = np.array(rysat)
rzsat = np.array(rzsat)
vxsat = np.array(vxsat)
vysat = np.array(vysat)
vzsat = np.array(vzsat)



plt.rcParams['figure.figsize'] = [10, 10]
plt.xlabel('rx [m]')
plt.ylabel('ry [m]')
plt.title('x-y Plane')
plt.plot(rxsat, rysat, label = 'Satellite')
plt.plot(rxearth, ryearth, label = 'Earth')

plt.legend()
plt.show()


plt.rcParams['figure.figsize'] = [10, 10]
plt.xlabel('ry [m]')
plt.ylabel('rz [m]')
plt.title('y-z Plane')
plt.plot(rysat, rzsat, label = 'Satellite')
plt.plot(ryearth, rzearth, label = 'Earth')

plt.legend()
plt.show()

plt.rcParams['figure.figsize'] = [10, 10]
plt.xlabel('rx [m]')
plt.ylabel('rz [m]')
plt.title('x-z Plane')
plt.plot(rxsat, rzsat, label = 'Satellite')
plt.plot(rxearth, rzearth, label = 'Earth')

plt.legend()
plt.show()


plt.plot(rxsat, rysat)
plt.show()

plt.plot(rxsat, rzsat)
plt.show()

from mpl_toolkits.mplot3d import Axes3D
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot(rxsat/1000, rysat/1000, rzsat/1000, label = 'Satellite')
ax.plot(rxearth/1000, ryearth/1000, rzearth/1000, label = 'Earth')
ax.set_title('3d Earth System')
ax.set_xlabel('rx [km]')
ax.set_ylabel('ry [km]')
ax.set_zlabel('rz [km]')
plt.legend()
plt.savefig('data/earth-euler-3d')
plt.show()


############
#Animated Plot
from matplotlib import animation
# Time Array


timesloose = []

for i in range(int(len(times)/1000)):
    timesloose.append(times[i*1000])

timesloose = np.array(timesloose)

rxearthloose = []
ryearthloose = []
rzearthloose = []

for i in range(int(len(times)/1000)):
    rxearthloose.append(rxearth[i*1000])
    ryearthloose.append(ryearth[i*1000])
    rzearthloose.append(rzearth[i*1000])

rxearthloose = np.array(rxearthloose)
ryearthloose = np.array(ryearthloose)
rzearthloose = np.array(rzearthloose)

rxsatloose = []
rysatloose = []
rzsatloose = []

for i in range(int(len(times)/1000)):
    rxsatloose.append(rxsat[i*1000])
    rysatloose.append(rysat[i*1000])
    rzsatloose.append(rzsat[i*1000])

rxsatloose = np.array(rxsatloose)
rysatloose = np.array(rysatloose)
rzsatloose = np.array(rzsatloose)

# Setting up Data Set for Animation
t = timesloose
datasetearth = np.array([rxearthloose/1000, ryearthloose/1000, rzearthloose/1000])
datasetsat = np.array([rxsatloose/1000, rysatloose/1000, rzsatloose/1000])
numDataPoints = len(t)

def animate_func(num):
    ax.clear()  # Clears the figure to update the line, point,   
                # title, and axes
    # Updating Trajectory Line (num+1 due to Python indexing)
    ax.plot3D(datasetearth[0, :num+1], datasetearth[1, :num+1], 
              datasetearth[2, :num+1], c='green')
    ax.plot3D(datasetsat[0, :num+1], datasetsat[1, :num+1], 
              datasetsat[2, :num+1], c='orange')
    # Updating Point Location 
    ax.scatter(datasetearth[0, num], datasetearth[1, num], datasetearth[2, num], 
               c='green', marker='o')
    ax.scatter(datasetsat[0, num], datasetsat[1, num], datasetsat[2, num], 
               c='orange', marker='o')
    # Adding Constant Origin
    ax.plot3D(datasetearth[0, 0], datasetearth[1, 0], datasetearth[2, 0],     
               c='black', marker='o')
    ax.plot3D(datasetsat[0, 0], datasetsat[1, 0], datasetsat[2, 0],     
               c='black', marker='o')
    # Setting Axes Limits
    ax.set_xlim3d([-500000, 400000])
    ax.set_ylim3d([-400000, 400000])
    ax.set_zlim3d([-400000, 300000])

    # Adding Figure Labels
    ax.set_title('Trajectory \nTime = ' + str(np.round(t[num],    
                 decimals=2)) + ' sec')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')

# Plotting the Animation
fig = plt.figure()
ax = plt.axes(projection='3d')
line_ani = animation.FuncAnimation(fig, animate_func, interval=100, frames=numDataPoints)
# f = r"data/earth-animation.gif"
# writergif = animation.PillowWriter(fps=numDataPoints)
# line_ani.save(f, writer=writergif)

plt.show()
