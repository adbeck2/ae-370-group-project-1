#Simple check that collision is working
import numpy as np
from core_functions import *

masses = [5.972e24, 419725]
radii = [6378100, 50]

u_0 = np.array([[0,0,0,0,0,0], [414000 + 6378100, 0, 0, 0, 0, 0]])

T = 2360600

delta_t = 10

u, times = ivp_RK4(u_0, T, delta_t, masses, radii=radii)